#Author:fgomezv@choucairtesting.com
@Regresion
Feature: Interactuar y conocer los diferentes controles de JavaScript

  @ControlesJavaScript
  Scenario: Interactuar con los diferentes controles de JavaScript
    Given que el usuario ingresa a la página de HeroKuapp
    When selecciona el link javascript_alerts
    And interactúa con los controles que hay en esta pantalla
    Then Aprende a manejar Alertas tipo JavaScript

