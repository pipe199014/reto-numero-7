package com.choucair.formacion.definition;

import com.choucair.formacion.steps.JavaSStep;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class JavaSDefinition {

	@Steps
	JavaSStep javaSStep;

	@Given("^que el usuario ingresa a la página de HeroKuapp$")
	public void que_el_usuario_ingresa_a_la_página_de_HeroKuapp() throws Throwable {
		javaSStep.ingresarPagina();
	}

	@When("^selecciona el link javascript_alerts$")
	public void selecciona_el_link_javascript_alerts() throws Throwable {
		javaSStep.ingresarAlLink();
	}

	@And("^interactúa con los controles que hay en esta pantalla$")
	public void interactúa_con_los_controles_que_hay_en_esta_pantalla() throws Throwable {
		javaSStep.interactuarJSAlert();
		Thread.sleep(2000);
		javaSStep.interactuarJSConfirm();
		Thread.sleep(3000);
		javaSStep.interactuarJSPrompt();
	}

	@Then("^Aprende a manejar Alertas tipo JavaScript$")
	public void aprende_a_manejar_Alertas_tipo_JavaScript() throws Throwable {

	}

}
