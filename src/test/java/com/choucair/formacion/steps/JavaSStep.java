package com.choucair.formacion.steps;

import org.fluentlenium.core.annotation.Page;

import com.choucair.formacion.pageobjects.JavaSPage;

public class JavaSStep {
	
	@Page
	JavaSPage javaSPage;
	
	public void ingresarPagina()
	{
		javaSPage.open();
	}
	
	public void ingresarAlLink()
	{		
		javaSPage.ingresarAlLink();
	}
	
	public void  interactuarJSAlert()
	{
		javaSPage.interactuarJSAlert();
	}
	
	public void interactuarJSConfirm()
	{
		javaSPage.interactuarJSConfirm();
	}
	
	public void interactuarJSPrompt() throws InterruptedException
	{
		javaSPage.interactuarJSPrompt();
	}

}
