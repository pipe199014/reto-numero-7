package com.choucair.formacion.pageobjects;

import org.openqa.selenium.Alert;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://the-internet.herokuapp.com/")
public class JavaSPage extends PageObject {

	@FindBy(css = "a[href='/javascript_alerts']")
	private WebElementFacade lnkJavaScript;

	// @FindBy(xpath = "//*[@id=\"content\"]/ul/li[25]/a")
	// private WebElementFacade lnkJavaScript;

	@FindBy(xpath = "//*[@id=\"content\"]/div/ul/li[1]/button")
	private WebElementFacade btnJsAlert;

	@FindBy(xpath = "//*[@id=\"content\"]/div/ul/li[2]/button")
	private WebElementFacade btnJsConfirm;

	@FindBy(xpath = "//*[@id=\"content\"]/div/ul/li[3]/button")
	private WebElementFacade btnJsPrompt;

	public void ingresarAlLink() {
		lnkJavaScript.click();
	}

	public void interactuarJSAlert() {
		btnJsAlert.click();

		Alert alert = this.getDriver().switchTo().alert();
		alert.accept();

	}

	public void interactuarJSConfirm() {
		btnJsConfirm.click();

		Alert confirm = this.getDriver().switchTo().alert();
		confirm.dismiss();
	}

	public void interactuarJSPrompt() throws InterruptedException {
		btnJsPrompt.click();
		Thread.sleep(2000);
		this.getDriver().switchTo().alert().sendKeys("Probando 123...");
		Alert prompt = this.getDriver().switchTo().alert();
		prompt.accept();
	}

}
